# eKréta Matrix bot



## Getting started

1. Download the [kreta.py](https://gitlab.com/raywiss/ekreta-matrix-bot/-/raw/main/kreta.py) and [kreat.ini-template](https://gitlab.com/raywiss/ekreta-matrix-bot/-/raw/main/kreta.ini-template) and put it into one directory.
1. `pip3 install matrix-nio`
1. Rename `kreta.ini-template` to `kreta.ini` and fill in
1. Grab the lib file called `kreta_v2.py` from [here](https://raw.githubusercontent.com/bczsalba/ekreta-docs-v3/bb307c0f37da5f9845db83b2ef9ed496347f6845/kreta_v2.py) and put in the same directory as your `kreta.py` is. Credit: [bczsalba](https://github.com/bczsalba/ekreta-docs-v3)

If you face connection issues try to change the line 9 in `kreta_v2.py` to

```
        return f"https://{ist}.e-kreta.hu"
```

## Usage

`./kreta.py gyerek1`

## Messages

# Hiányzás
## Tanuló: Gyerek Neve

**Tantárgy**: Angol nyelv

**Dátum**: 2021.09.23. 11:00 - 2021.09.23. 11:45

# Bejelentés
## Írásbeli témazáró dolgozat
### Tanuló: Gyerek Neve

**Tantárgy:** Angol nyelv

**Téma:** Év eleji dolgozat

**Mód:** Írásbeli témazáró dolgozat

**Dátum:** 2021.09.27.

# Értékelés
## Tanuló: Gyerek Neve

**Tantárgy:** Angol nyelv

**Téma:** My holiday

**Osztályzat:** Jeles(5)

**Súly:** 100%

**Mód:** Szóbeli felelet

**Dátum:** 2021-09-08T22:00:00Z
